// node bigquerydump.js --projectId=vangvietnghi-new --keyFileName='./vangvietnghi-52b1326abd21.json' --output='./big.json' --query='SELECT * FROM [bigquery-public-data:san_francisco.bikeshare_trips] LIMIT 100' --excludingFields='duration_sec'
var argv = require('optimist').argv
var stringify = require('json-stable-stringify');

var projectId = argv.projectId;
var keyFileName = argv.keyFileName;
var query = argv.query;
var output = argv.output;
var excludingFields = [];
if( argv.excludingFields &&  argv.excludingFields.length != 0)
{
  excludingFields = argv.excludingFields.split(',');
}
var bigquery = require('@google-cloud/bigquery')({
  'projectId': projectId,
  'keyFilename': keyFileName
});
var fs = require('fs');

var result = [];

// query = "SELECT * FROM [bigquery-public-data:san_francisco.bikeshare_trips] LIMIT 100000"

function removeField(json,exfields){
    for(var i=0; i < exfields.length; i++){
      delete json[exfields[i]]
      // eval('delete json.'+excludingFields[i]);
    }
}

if(type == 'data'){
  var writer = fs.createWriteStream(output)
  var sum = 0;
  bigquery.createQueryStream(query)
    .on('error', function(err){
      //console.error(err);
      writer.end();
    })
    .on('data', function(row) {
        removeField(row,excludingFields);
        writer.write(stringify(row)+"\n");
        sum++;
        if(process){
          process.send(sum);
        }
      // row is a result from your query.
    })
    .on('end', function() {
      // All rows retrieved.
       writer.end();
    });
}

if(type == 'count'){

  bigquery.createQueryStream(query)
    .on('error', function(err){
      //console.error(err);
      writer.end();
    })
    .on('data', function(row) {
        writer.write(stringify(row)+"\n");
        sum++;
      // row is a result from your query.
    })
    .on('end', function() {
      // All rows retrieved.
       writer.end();
    });
}
