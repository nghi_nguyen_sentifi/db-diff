
var fork = require('child_process').fork;
var exec = require('child_process').exec;
var execFile = require('child_process').execFile;
var shelljs = require('shelljs');
var fs = require('fs');
var elasticsearch = require('elasticsearch');
var ProgressBar = require('console-progress');
var jsondiff = require('jsondiffpatch');

var dbdiff = function(options){
    var self = this;
    self.options = options;
    if(!self.options.type) self.options.type = 'elasticsearch';
    // validate and assign options
    self.validationErrors = self.validateOptions(options);
    self.progress = {};
    self.progress.left = [];
    self.progress.right = [];

    self.progress.show = function(){
      for(var i=0; i < self.progress.left.length; i++){
          if(self.progress.left[i])
           console.log("Left["+i+"] : " + self.progress.left[i].current +"/" +self.progress.left[i].total )
      }
      for(var j=0; j < self.progress.right.length; j++){
        if(self.progress.right[j])
          console.log("Right["+j+"] : "+self.progress.right[j].current +"/" +self.progress.right[j].total)
      }
      console.log('-------------------------------------  ')
    }

    self.progress.hide = function(){

    }

    self.progress.updateTotal = function(side, index, total){
      if(!self.progress[side][index])
         self.progress[side][index] = {};

      self.progress[side][index].total = total;
      self.progress[side][index].current = 0;

    }

    self.progress.update = function(side, index, current){
      if(!self.progress[side][index])
          self.progress[side][index] = {};

       self.progress[side][index].current = current;
       //self.progress[side][index].progressBar.tick(current);
    }

    self.progress.error = function(side, index, state){
      if(!self.progress[side][index])
          self.progress[side][index] = {};

      if(state)
          self.progress[side][index].state = state;
      self.progress[side][index].error = true;
    }

}

dbdiff.prototype.doAction = function(){
  var self = this;
  shelljs.rm('-rf',self.options.output);
  if(!Array.isArray(self.options.searchBody) && self.options.searchBody)
      options.searchBody = [options.searchBody];

  var timer = setInterval(function(){
      self.progress.show();
  }, 5000);

  // download  all and sort
  var ps = [];
  for(var i = 0 ; i < self.options.left.searchBody.length; i++){
      var index = i;
      var p = self.count("left",index)
              .then(function(result){
                self.progress.updateTotal('left',index,result);
                return self.dump("left",index, function(number){
                  self.progress.update('left',index,number);
                });
              })
              .then(function(result){
                console.log('download file : ' + result+ ' -> done. Now sort')
                return self.sort(result);
              })
              .then(function(result){
                console.log('sort file : ' + result+ ' -> done.')
              }).catch(function(error){
                  console.log("error on file")
                  self.progress.error('left',index);
                  process.exit(-1);
              })
       ps.push(p);
  }

  for(var j = 0 ; j < self.options.right.searchBody.length; j++){
    var index = j;
    var p = self.count("right",index)
                .then(function(result){
                  self.progress.updateTotal('right',index,result);
                   return self.dump("right",index, function(number){
                     self.progress.update('right',index,number);
                   });
                })
                .then(function(result){
                  console.log('download file : ' + result+ ' -> done. Now sort')
                  return self.sort(result);
                })
                .then(function(result){
                  console.log('sort file : ' + result+ ' -> done.')
                }).catch(function(error){
                  console.log("error on file")
                  self.progress.error('sort',index);
                  process.exit(-1);
                })
    ps.push(p);
  }

  Promise.all(ps).then(function(result){
      self.progress.show();
      clearInterval(timer)
      console.log('compare 2 folder '+self.options.output+'/left and '+ self.options.output+'/right')
      self.progress.hide();
      self.diffFolder(self.options.output+'/left', self.options.output+'/right')
          .then(function(result){
            console.log(" RESULT "+result)
             return self.processDiff(result);
          })
          .then(function(result){
            return new Promise(function(resolve,reject){
              if(result.left.length == 0 && result.right.length == 0){
                console.log("2 DB ARE SAME !!! CONGRATULATION !")
                return ;
              }
              var diff = jsondiff.diff(result.left, result.right);
              if(!diff){
                console.log("2 DB ARE SAME !!! CONGRATULATION !")
                return;
              }

              console.log("2 DB ARE NOT SAME !!")

              var added = [];
              var removed = []
              var modified = {};

              for(var k in diff){
                var edit = diff[k]
                if(!k.startsWith('_')){
                  if(Array.isArray(edit)){
                    added.push(edit);
                  }else{
                    modified[k] = diff[i];
                  }
                }else{
                  if(!k.startsWith('_t')){
                    removed.push(edit[0]);
                  }
                }
              }
              console.log('-------Summary--------');
              console.log('No added docs : '+ added.length);
              console.log('No removed docs : ' + removed.length);
              var keys = Object.keys(modified);

              console.log('No modified properties : '+ keys.length);
              if(keys.length != 0){
                  console.log('Some modified properties .... ')
                  for(var i=0; i < Math.min(keys.length,10); i++){
                      console.log(keys[i] +" : " + modified[keys[i]]);
                  }
              }

              fs.writeFileSync(self.options.output+'/left-diff', result.left);
              fs.writeFileSync(self.options.output+'/right-diff', result.right);

            })
          }).catch(function(err){
               console.log(err);
               process.exit(1);
          })
  }).catch(function(err){
       console.log(err);
       process.exit(1);
  })
}


dbdiff.prototype.validateOptions = function(options){

}


dbdiff.prototype.dump = function(side,index, callback){
   if(this.options[side].type == 'elasticsearch') return this.dumpEs(side,index, callback);
   if(this.options[side].type == 'bigquery') return this.dumpBigQuery(side,index, callback);
   if(this.options[side].type == 'mongodb') return this.dumpMongo(side,index, callback);

}

dbdiff.prototype.count = function(side,index){
   if(this.options[side].type == 'elasticsearch') return this.countEs(side,index);
   if(this.options[side].type == 'bigquery') return this.countBigQuery(side,index);
   if(this.options[side].type == 'mongodb') return this.countMongo(side,index);
}

dbdiff.prototype.countEs = function(side, index, callback){
   var options = this.options;
    return new Promise(function(resolve,reject){
      var client = new elasticsearch.Client({
          host: options[side].url,
        });

        client.count({
          index : options[side].index,
          body: options[side].searchBody[index]
        }).then(function (body) {
          resolve(body.count);
        }, function (error) {
          console.trace(error.message);
          reject(error);
        });
    })
}

dbdiff.prototype.dumpEs = function(side, index, callback){
  var options = this.options;
  shelljs.mkdir('-p',  options.output +'/' + side );
  console.log('dump from Elastic: ' + options.output +'/' + side +'/'+ index+'.json')
  return new Promise(function(resolve,reject){

    var excludeFieldStr = "";
    for(var i=0; i < options[side].excludingFields.length;i++){
          delstr = 'delete doc._source.' + options[side].excludingFields[i] + ';'
          excludeFieldStr += delstr;
    }

    var child_process = fork('./elasticdump.js', [
      '--type=' + 'data',
      '--url=' + options[side].url,
      '--index=' + options[side].index,
      '--output=' + options.output +'/' + side +'/'+ index+'.json',
      '--scrollTime=' + options[side].scrollTime,
      '--limit=' + options[side].limit,
      '--excludingFields='+options[side].excludingFields,
      '--sourceOnly=true',
      '--searchBody=' + JSON.stringify(options[side].searchBody[index])
    ])

    child_process.on('close', function (code) {
      if (code !== 0) {
          reject(code)
      } else {
          resolve(options.output +'/' + side +'/'+ index+'.json');
      }
    })

    child_process.on('message', function(message){
      // progress
       callback(message);
    });
  })
}


dbdiff.prototype.dumpBigQuery = function(side, index, callback){
  var options = this.options;
  shelljs.mkdir('-p',  options.output +'/' + side );
  console.log('dump from BigQuery: ' + options.output +'/' + side +'/'+ index+'.json')
  return new Promise(function(resolve,reject){

    // node bigquerydump.js --projectId=vangvietnghi-new --keyFileName='./vangvietnghi-52b1326abd21.json' --outputFile='./big.json' --query='SELECT * FROM [bigquery-public-data:san_francisco.bikeshare_trips] LIMIT 100' --excludingFields='duration_sec'


    var child_process = fork('./bigquerydump.js', [
      '--projectId='+options[side].projectId,
      '--keyFileName=' + options[side].keyFileName,
      '--output=' + options.output +'/' + side +'/'+ index+'.json',
      '--excludingFields='+options[side].excludingFields,
      '--query=' + options[side].searchBody[index]
    ])

    child_process.on('close', function (code) {
      if (code !== 0) {
          reject(code)
      } else {
          resolve(options.output +'/' + side +'/'+ index+'.json');
      }
    })

    child_process.on('message', function(message){
      callback(message);
    });
  })
}

dbdiff.prototype.countBigQuery = function(side, index, callback){
    return new Promise(function(resolve,reject){
        resolve(0);
    })
}


dbdiff.prototype.dumpMongo = function(side, index, callback){
  var options = this.options;
  shelljs.mkdir('-p',  options.output +'/' + side );
  console.log('dump from Mongodb: ' + options.output +'/' + side +'/'+ index+'.json')
  return new Promise(function(resolve,reject){

    // node bigquerydump.js --projectId=vangvietnghi-new --keyFileName='./vangvietnghi-52b1326abd21.json' --outputFile='./big.json' --query='SELECT * FROM [bigquery-public-data:san_francisco.bikeshare_trips] LIMIT 100' --excludingFields='duration_sec'
    // console.log(options[side].searchBody[index]);

    var child_process = fork('./mongodump.js', [
      '--url='+options[side].url,
      '--collection=' + options[side].collection,
      '--output=' + options.output +'/' + side +'/'+ index+'.json',
      '--type=data',
      '--excludingFields='+options[side].excludingFields,
      '--query=' + options[side].searchBody[index]
    ])

    child_process.on('close', function (code) {
      if (code !== 0) {
          reject(code)
      } else {
          resolve(options.output +'/' + side +'/'+ index+'.json');
      }
    })

    child_process.on('message', function(message){
      callback(message);
    });
  })
}


dbdiff.prototype.countMongo = function(side, index){
  var options = this.options;
  //shelljs.mkdir('-p',  options.output +'/' + side );
  //console.log('dump from Mongodb: ' + options.output +'/' + side +'/'+ index+'.json')
  return new Promise(function(resolve,reject){

    // node bigquerydump.js --projectId=vangvietnghi-new --keyFileName='./vangvietnghi-52b1326abd21.json' --outputFile='./big.json' --query='SELECT * FROM [bigquery-public-data:san_francisco.bikeshare_trips] LIMIT 100' --excludingFields='duration_sec'
    // console.log(options[side].searchBody[index]);

    var child_process = fork('./mongodump.js', [
      '--url='+options[side].url,
      '--collection=' + options[side].collection,
      '--output=' + options.output +'/' + side +'/'+ index+'.json',
      '--type=count',
      '--excludingFields='+options[side].excludingFields,
      '--query=' + options[side].searchBody[index]
    ])

    var count = 0;
    child_process.on('close', function (code) {
      if (code !== 0) {
          reject(code)
      } else {
          resolve(count);
      }
    })

    child_process.on('message', function(message){
      // progress
      count = message;
    });
  })
}


dbdiff.prototype.sort = function(filePath){
  console.log("sorting file : "+ filePath);
  return new Promise(function(resolve, reject){
     shelljs.exec('sort '+filePath+' -o '+filePath, {maxBuffer: 1024 * 1024 * 10}, function(error, stdout, stderr){
       if(error){
         console.log(error);
         reject(stderr)
       }else{
         console.log(stdout);
         resolve(filePath);
       }
     });
  });
}

dbdiff.prototype.diffFile = function(file1, file2){
  return new Promise(function(resolve, reject){
    shelljs.exec('diff '+file1+' '+file2, {maxBuffer: 1024 * 1024 * 100}, function(error, stdout, stderr){
       if(error){
         console.log(error);
         reject(stderr)
       }else{
         console.log(stdout);
         resolve(stdout);
       }
     });
  });
}

dbdiff.prototype.diffFolder = function(path1, path2){
  var options = this.options;
  return new Promise(function(resolve, reject){
    shelljs.silent = true;
    try{
      shelljs.exec('diff -r '+path1+' '+path2+' > ' + options.output + '/diff', function(error, stdout, stderr){
          if(error){
            console.log(stderr);
            reject(stderr)
          }else{
            console.log(stdout);
            resolve(stdout);
          }
      });
      resolve(options.output + '/diff');
    }catch(e){
      reject(e)
    }
  })
}

dbdiff.prototype.processDiff = function(diffContent){
  return new Promise(function(resolve,reject){
      fs.readFile(diffContent, 'utf8', function (err,data) {
          if(err){
            reject(err);
          }else{
              var obj = {};
              obj.left = [];
              obj.right = [];
              var lines = data.split('\n');
              for(var i=0; i< lines.length; i++){
                 var line = lines[i];
                 if(line.startsWith('<')){
                     obj.left.push(line.replace('< ',''));
                 }else if(line.startsWith('>')){
                      obj.right.push(line.replace('> ',''));
                 }
              }
              resolve(obj);
          }
      })
  })
  return diffContent;
}


module.exports = dbdiff
