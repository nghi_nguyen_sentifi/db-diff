var config = {};
var options = {}

/*-------Config ---*/
options.left = {
    'type' : 'mongodb',
    'url'  : 'mongodb://relevant.sen:27017/analytic2',
    'collection' : 'relevant',
    'excludingFields' : ['updated_at'],
    'searchBody' : [
      "{_id : {$gt : ObjectId('58e3d1100000000000000000'),$lt : ObjectId('58e3d3680000000000000000')}}"
    ]
};

options.right = {
    'type' : 'mongodb',
    'url'  : 'mongodb://relevant.sen:27017/analytic2',
    'collection' : 'relevant',
    'excludingFields' : ['updated_at'],
    'searchBody' : [
      "{_id : {$gt : ObjectId('58e3d1100000000000000000'), $lt : ObjectId('58e3d3680000000000000000')}}"
    ]
};
options.output = 'relevant'

/*---------------------*/

config.options = options;
module.exports = config
