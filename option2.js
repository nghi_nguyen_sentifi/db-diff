var config = {};
var options = {}

/*-------Config ---*/
options.left = {
    'type' : 'bigquery',
    'projectId'  : 'vangvietnghi-new',
    'keyFileName' : './vangvietnghi-52b1326abd21.json',
    'excludingFields' : ['start_station_name','start_station_id'],
    'offset' : 0,
    'searchBody' : [
        'SELECT trip_id,duration_sec FROM [bigquery-public-data:san_francisco.bikeshare_trips] LIMIT 100'
    ]
};

options.right = {
    'type' : 'bigquery',
    'projectId'  : 'vangvietnghi-new',
    'keyFileName' : './vangvietnghi-52b1326abd21.json',
    'excludingFields' : ['start_station_name','start_station_id'],
    'offset' : 0,
    'searchBody' : [
        'SELECT * FROM [bigquery-public-data:san_francisco.bikeshare_trips] LIMIT 102'
    ]
};
options.output = 'bikeshare'

/*---------------------*/

config.options = options;
module.exports = config
