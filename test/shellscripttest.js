'use strict';

describe('shell-script-test', function () {
  it('can create folder', function (done) {
    var shelljs = require('shelljs');
    var fs = require('fs');
    var assert = require("assert");
    var path = './testmkdir-'+ Date.now();
    shelljs.mkdir('-p', path);
    assert.equal(true,fs.existsSync(path));
    // must exist folder after create
    shelljs.rm('-rf', path);
    done();
  });

  it('can create and write file', function (done) {
    var shelljs = require('shelljs');
    var fs = require('fs');
    var assert = require("assert");
    var path  = './testfile-'+ Date.now();
    var filePath = path +'/hello';
    shelljs.mkdir('-p', path);
    shelljs.exec('echo -n hello-world > '+filePath);
    var message = fs.readFileSync(filePath,'utf8');
    assert.equal('hello-world',message);
    // must exist file and have content
    shelljs.rm('-rf', path);
    done();
  });


  it('can sort file', function (done) {
    var shelljs = require('shelljs');
    var fs = require('fs');
    var assert = require("assert");
    var path  = './testfile-'+ Date.now();
    var filePath = path +'/hello';
    var sortedFruit = ['apple','banana','organge',''];

    shelljs.mkdir('-p', path);
    shelljs.exec('echo organge >> '+filePath);
    shelljs.exec('echo apple >> '+filePath);
    shelljs.exec('echo -n banana >> '+filePath);
    shelljs.exec('sort '+filePath+' -o '+ filePath);

    var message = fs.readFileSync(filePath,'utf8');
    assert.deepEqual(sortedFruit,message.split('\n'));
    // must exist file, sorted content
    shelljs.rm('-rf', path);
    done();
  });

  it('can run diff command', function (done) {
    var shelljs = require('shelljs');
    var fs = require('fs');
    var assert = require("assert");
    var path  = './testfile-'+ Date.now();
    var file1 = path +'/file1';
    var file2 = path +'/file2';
    var diffFile = path +'/diff';

    shelljs.mkdir('-p', path);
    shelljs.exec('echo -n hello-world1 > '+file1);
    shelljs.exec('echo -n hello-world2 > '+file2);
    shelljs.exec('diff -n '+file1+' '+file2+' > '+diffFile);

    var message = fs.readFileSync(diffFile,'utf8');
    assert.equal(true,message != null);
    // must exist diff file and not null content
    shelljs.rm('-rf', path);
    done();
  });
});
