'use strict';

var should = require('should');
var jsondiffpatch = require('jsondiffpatch');

describe('diff-test', function () {
  it('should return no diff when shuffle order of properties', function (done) {
    var country = {
            name: "Argentina",
            capital: "Buenos Aires",
            independence: new Date(1816, 6, 9),
            unasur: true
        };
    var country2 = {
        independence: new Date(1816, 6, 9),
        name: "Argentina",
        unasur: true,
        capital: "Buenos Aires"
    };
    var diff = jsondiffpatch.diff(country,country2);
    should.not.exist(diff);
    done();
  });

  it('should return no diff when change order of element in array', function (done) {
    var fruit1 = {
      name : "Apple",
      size : "big",
      price : 100
    }

    var fruit2 = {
      name : "Orange",
      size : "small",
      price : 300
    }
    var fruitList1 = [fruit1,fruit2];
    var fruitList2 = [fruit2,fruit1];
    var diff = jsondiffpatch.diff(fruitList1,fruitList2);
    should.deepEqual(diff, {
        '_t' : 'a',
        '_1' : ['', 0, 3]
    });
    done();
  });

  it('should return added json', function (done) {
    var fruit1 = {
      name : "Apple",
      size : "big",
      price : 100
    }

    var fruit2 = {
      name : "Orange",
      size : "small",
      price : 300
    }
    var fruitList1 = [fruit2];
    var fruitList2 = [fruit1,fruit2];
    var diff = jsondiffpatch.diff(fruitList1,fruitList2);
    should.deepEqual(diff, {
        '_t' : 'a',
        '0' : [{
          name : "Apple",
          size : "big",
          price : 100
        }]
    });
    done();
  });

  it('should return removed json', function (done) {
    var fruit1 = {
      name : "Apple",
      size : "big",
      price : 100
    }

    var fruit2 = {
      name : "Orange",
      size : "small",
      price : 300
    }
    var fruitList1 = [fruit2,fruit1];
    var fruitList2 = [fruit1];
    var diff = jsondiffpatch.diff(fruitList1,fruitList2);
    should.deepEqual(diff, {
        '_t' : 'a',
        '_0' : [{
          name : "Orange",
          size : "small",
          price : 300
        },0,0]
    });
    done();
  });

  it('should return show diff and modified property', function (done) {
    var country = {
        independence: new Date(1816, 6, 9),
        name: "Argentina",
        unasur: true,
        capital: "Buenos Aires"
    };

    var country2 = {
        independence: new Date(1816, 6, 9),
        name: "Argentina",
        unasur: true,
        capital: "Buenos Aires 2"
    };
    var diff = jsondiffpatch.diff(country,country2);
    should.exist(diff);
    should.deepEqual(diff.capital,["Buenos Aires", "Buenos Aires 2"])
    done();
  });


});
