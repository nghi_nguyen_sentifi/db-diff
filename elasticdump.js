// node bigquerydump.js --projectId=vangvietnghi-new --keyFileName='./vangvietnghi-52b1326abd21.json' --output='./big.json' --query='SELECT * FROM [bigquery-public-data:san_francisco.bikeshare_trips] LIMIT 100' --excludingFields='duration_sec'
var argv = require('optimist').argv
var elasticsearch = require('elasticsearch');
var ElasticsearchScrollStream = require('elasticsearch-scroll-stream');
var stringify = require('json-stable-stringify');

var url = argv.url;
var collection = argv.collection;
var query = argv.query;
var index = argv.index;
var output = argv.output;
var scrollTime = argv.scrollTime;
var searchBody = argv.searchBody;
var type = argv.type ? argv.type : 'data';
var excludingFields = [];
if( argv.excludingFields &&  argv.excludingFields.length != 0)
{
  excludingFields = argv.excludingFields.split(',');
}
var fs = require('fs');
const vm = require('vm');

var result = [];

var client = new elasticsearch.Client({
    host: url
});

function removeField(json,exfields){
    for(var i=0; i < exfields.length; i++){
      delete json[exfields[i]]
    }
}

if(type == 'data'){

  var es_stream = new ElasticsearchScrollStream(client, {
    index: index,
    scroll: scrollTime,
    body : JSON.parse(searchBody)
  });

  var sum = 0;
  var writer = fs.createWriteStream(output);
  es_stream.on('data', function(row) {
        var json = JSON.parse(row.toString());
        sum++;
        removeField(json,excludingFields);
        writer.write(stringify(json)+"\n");
        if(process){
          process.send(sum);
        }
      // row is a result from your query.
    })
    .on('end', function() {
      // All rows retrieved.
       writer.end();
    })
    .on('error', function(err) {
      console.log(err);
      process.exit(-1);
    });
}

if(type == 'count'){

}
