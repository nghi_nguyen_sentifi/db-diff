# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This tool is use for checking the difference between 2 db (ElasticSearch, MongoDB
* 1.0.0 

### How do I get set up? ###

* Install dependency module 
```
#!bash

npm install 
```
* See example in option1.js

```
#!javascript

var config = {};
var options = {}

/*-------Config ---*/
options.type = "elasticsearch"
options.left = 'http://es-int-client-1.senvpc:9200/rm_feed_staging'
options.right = 'http://es-staging-client-1.senvpc:9200/rm_feed_staging'
options.output = 'rm_feed_staging'
options.scrollTime = '10m'
options.limit = 5000
options.offset = 0;
options.searchBody = [
  {"query":{"filtered":{"filter":{"range":{"published_at":{"gte":"2017-04-22T00:00:00","lt":"2017-04-22T00:10:00"}}}}}},
  {"query":{"filtered":{"filter":{"range":{"published_at":{"gte":"2017-04-22T00:10:00","lt":"2017-04-22T00:20:00"}}}}}},
  {"query":{"filtered":{"filter":{"range":{"published_at":{"gte":"2017-04-22T00:20:00","lt":"2017-04-22T00:30:00"}}}}}},
  {"query":{"filtered":{"filter":{"range":{"published_at":{"gte":"2017-04-22T00:30:00","lt":"2017-04-22T00:40:00"}}}}}},
  {"query":{"filtered":{"filter":{"range":{"published_at":{"gte":"2017-04-22T00:40:00","lt":"2017-04-22T00:50:00"}}}}}},
  {"query":{"filtered":{"filter":{"range":{"published_at":{"gte":"2017-04-22T00:50:00","lt":"2017-04-22T01:00:00"}}}}}},
];
/*---------------------*/

config.options = options;
module.exports = config

```
* Run command to check 

```
#!bash

./dbdiff --optionFile=./option1.js
```
* Dependencies

```
#!javascript

"dependencies": {
    "elasticdump": "^3.2.0",
    "shelljs" : "0.7.7",
    "ascii-progress" : "1.0.5"
  },
```

* How to run tests
* Deployment instructions