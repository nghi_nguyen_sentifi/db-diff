var config = {};
var options = {}

/*-------Config ---*/
options.left = {
    'type' : 'elasticsearch',
    'url'  : 'http://es-int-client-1.senvpc:9200',
    'index' : 'rm_feed_staging',
    'scrollTime' : '10m',
    'limit' : 1000,
    'excludingFields' : ['updated_at'],
    'offset' : 0,
    'searchBody' : [
      {"query":{"filtered":{"filter":{"range":{"created_at":{"gte":"2017-04-10T00:00:00","lte":"2017-04-10T00:00:10"}}}}}}
    ]
};

options.right = {
    'type' : 'elasticsearch',
    'url'  : 'http://es-int-client-1.senvpc:9200',
    'index' : 'rm_feed',
    'scrollTime' : '10m',
    'limit' : 1000,
    'excludingFields' : ['updated_at'],
    'offset' : 0,
    'searchBody' : [
        {"query":{"filtered":{"filter":{"range":{"created_at":{"gte":"2017-04-10T00:00:00","lte":"2017-04-10T00:00:10"}}}}}}
    ]
};
options.output = 'rm_feed_staging'

/*---------------------*/

config.options = options;
module.exports = config
