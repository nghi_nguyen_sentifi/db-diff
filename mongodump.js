// node bigquerydump.js --projectId=vangvietnghi-new --keyFileName='./vangvietnghi-52b1326abd21.json' --output='./big.json' --query='SELECT * FROM [bigquery-public-data:san_francisco.bikeshare_trips] LIMIT 100' --excludingFields='duration_sec'
var argv = require('optimist').argv
var stringify = require('json-stable-stringify');

var url = argv.url;
var collection = argv.collection;
var query = argv.query;
var output = argv.output;
var type = argv.type ? argv.type : 'data';
var excludingFields = [];
if( argv.excludingFields &&  argv.excludingFields.length != 0)
{
  excludingFields = argv.excludingFields.split(',');
}
var mongo = require('mongodb');
var fs = require('fs');
const vm = require('vm');

var result = [];

// query = "SELECT * FROM [bigquery-public-data:san_francisco.bikeshare_trips] LIMIT 100000"

function removeField(json,exfields){
    for(var i=0; i < exfields.length; i++){
      delete json[exfields[i]]
      // eval('delete json.'+excludingFields[i]);
    }
}

if(type == 'data'){
  var sum = 0;
  var writer = fs.createWriteStream(output)
  mongo.connect(argv.url, function(err,db){
      if(err){
        process.exit(-1);
        throw err;
      }

      var queryObj = new vm.Script('a='+query);
      queryObj = queryObj.runInContext(new vm.createContext({ObjectId:mongo.ObjectID}))
      db.collection(collection).find(queryObj).batchSize(1000).stream()
         .on('error', function(err){
          //console.error(err);
          writer.end();
        })
        .on('data', function(row) {
            sum++;
            removeField(row,excludingFields);
            writer.write(stringify(row)+"\n");
            if(process){
              process.send(sum);
            }
          // row is a result from your query.
        })
        .on('end', function() {
          // All rows retrieved.
           writer.end();
           db.close();
        });
  })
}

if(type == 'count'){
  mongo.connect(argv.url, function(err,db){
      if(err){
        throw err;
        process.exix(-1);
      }
      var queryObj = new vm.Script('a='+query);
      queryObj = queryObj.runInContext(new vm.createContext({ObjectId:mongo.ObjectID}))
      db.collection(collection).count(function(err, count){
          if(err){
            db.close();
            process.exit(-1);
          }
          if(process){
            process.send(count);
          }
          db.close();
          process.exit(0);
      })
  })
}
